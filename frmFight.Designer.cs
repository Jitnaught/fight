﻿namespace FIGHT
{
    partial class frmFight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnPickOne = new System.Windows.Forms.Button();
            this.btnPickTwo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(12, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnPickOne
            // 
            this.btnPickOne.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPickOne.Location = new System.Drawing.Point(12, 53);
            this.btnPickOne.Name = "btnPickOne";
            this.btnPickOne.Size = new System.Drawing.Size(357, 206);
            this.btnPickOne.TabIndex = 1;
            this.btnPickOne.UseVisualStyleBackColor = true;
            this.btnPickOne.Click += new System.EventHandler(this.btnPickOne_Click);
            // 
            // btnPickTwo
            // 
            this.btnPickTwo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickTwo.Location = new System.Drawing.Point(375, 53);
            this.btnPickTwo.Name = "btnPickTwo";
            this.btnPickTwo.Size = new System.Drawing.Size(357, 206);
            this.btnPickTwo.TabIndex = 2;
            this.btnPickTwo.UseVisualStyleBackColor = true;
            this.btnPickTwo.Click += new System.EventHandler(this.btnPickTwo_Click);
            // 
            // frmFight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 276);
            this.Controls.Add(this.btnPickTwo);
            this.Controls.Add(this.btnPickOne);
            this.Controls.Add(this.btnCancel);
            this.Name = "frmFight";
            this.Text = "FIGHT - Battle";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFight_FormClosed);
            this.Load += new System.EventHandler(this.frmFight_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnPickOne;
        private System.Windows.Forms.Button btnPickTwo;
    }
}