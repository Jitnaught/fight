﻿//The MIT License (MIT)
//
//Copyright(c) 2016 LetsPlayOrDy
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace FIGHT
{
    public partial class frmList : Form
    {
        public bool hiding = false;

        public List<string> list = new List<string>();

        public frmList()
        {
            InitializeComponent();
        }

        private void btnBrowseListFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            ofd.Filter = "Text Files|*.txt";
            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                tbList.Clear();
                StreamReader sr = File.OpenText(ofd.FileName);
                tbList.Text = sr.ReadToEnd();
                sr.Close();
            }
        }

        private bool anyLinesEmpty()
        {
            list.Clear();
            bool empty = true;
            foreach (string line in tbList.Lines)
            {
                if (!string.IsNullOrWhiteSpace(line))
                {
                    list.Add(line);
                    empty = false;
                }
            }

            return empty;
        }

        private void btnFight_Click(object sender, EventArgs e)
        {
            if (tbList.Lines.Length > 1 && !anyLinesEmpty())
            {
                hiding = true;
                this.ShowInTaskbar = false;
                this.Hide();
                frmFight form = new frmFight();
                form.RefToForm1 = this;
                form.Show();
            }
            else
            {
                MessageBox.Show("You haven't seperated your list or it is empty");
            }
        }

        private void frmList_Shown(object sender, EventArgs e)
        {
            if (hiding) this.Hide();

            this.ShowInTaskbar = !hiding;
        }
    }
}
