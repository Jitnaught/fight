﻿//The MIT License (MIT)
//
//Copyright(c) 2016 LetsPlayOrDy
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

using System;
using System.Windows.Forms;

namespace FIGHT
{
    public partial class frmFight : Form
    {
        public frmList RefToForm1 { get; set; }
        int first = 0;
        int second = 0;
        bool lastChosen = false;

        public frmFight()
        {
            InitializeComponent();
        }

        private void openListForm()
        {
            frmList listForm = RefToForm1;
            listForm.hiding = false;
            listForm.Show();
        }

        Random rand = new Random();

        private void doNextFight()
        {
            int count = RefToForm1.list.Count;
            if (count > 2)
            {
                first = rand.Next(count - 1);
                while ((second = rand.Next(count - 1)) == first) ;

                btnPickOne.Text = RefToForm1.list[first];
                btnPickTwo.Text = RefToForm1.list[second];
            }
            else if (count == 2)
            {
                first = 0;
                second = 1;

                btnPickOne.Text = RefToForm1.list[first];
                btnPickTwo.Text = RefToForm1.list[second];
            }
            else if (count == 1)
            {
                MessageBox.Show("\"" + (lastChosen ? btnPickTwo.Text : btnPickOne.Text) + "\" won!");
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            openListForm();
            Close();
        }

        private void frmFight_Load(object sender, EventArgs e)
        {
            doNextFight();
        }

        private void btnPickOne_Click(object sender, EventArgs e)
        {
            lastChosen = false;
            RefToForm1.list.RemoveAt(second);
            doNextFight();
        }

        private void btnPickTwo_Click(object sender, EventArgs e)
        {
            lastChosen = true;
            RefToForm1.list.RemoveAt(first);
            doNextFight();
        }

        private void frmFight_FormClosed(object sender, FormClosedEventArgs e)
        {
            openListForm();
        }
    }
}
