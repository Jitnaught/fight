﻿namespace FIGHT
{
    partial class frmList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBrowseListFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbList = new System.Windows.Forms.TextBox();
            this.btnFight = new System.Windows.Forms.Button();
            this.lblListLocation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBrowseListFile
            // 
            this.btnBrowseListFile.Location = new System.Drawing.Point(13, 13);
            this.btnBrowseListFile.Name = "btnBrowseListFile";
            this.btnBrowseListFile.Size = new System.Drawing.Size(94, 23);
            this.btnBrowseListFile.TabIndex = 0;
            this.btnBrowseListFile.Text = "Browse for list";
            this.btnBrowseListFile.UseVisualStyleBackColor = true;
            this.btnBrowseListFile.Click += new System.EventHandler(this.btnBrowseListFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "or write it out (seperate by new lines)";
            // 
            // tbList
            // 
            this.tbList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbList.Location = new System.Drawing.Point(12, 73);
            this.tbList.Multiline = true;
            this.tbList.Name = "tbList";
            this.tbList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbList.Size = new System.Drawing.Size(264, 226);
            this.tbList.TabIndex = 2;
            // 
            // btnFight
            // 
            this.btnFight.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnFight.Location = new System.Drawing.Point(99, 312);
            this.btnFight.Name = "btnFight";
            this.btnFight.Size = new System.Drawing.Size(75, 23);
            this.btnFight.TabIndex = 3;
            this.btnFight.Text = "Fight!";
            this.btnFight.UseVisualStyleBackColor = true;
            this.btnFight.Click += new System.EventHandler(this.btnFight_Click);
            // 
            // lblListLocation
            // 
            this.lblListLocation.AutoSize = true;
            this.lblListLocation.Location = new System.Drawing.Point(113, 18);
            this.lblListLocation.Name = "lblListLocation";
            this.lblListLocation.Size = new System.Drawing.Size(0, 13);
            this.lblListLocation.TabIndex = 4;
            // 
            // frmList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 347);
            this.Controls.Add(this.lblListLocation);
            this.Controls.Add(this.btnFight);
            this.Controls.Add(this.tbList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBrowseListFile);
            this.Name = "frmList";
            this.Text = "FIGHT - Input list";
            this.Shown += new System.EventHandler(this.frmList_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowseListFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbList;
        private System.Windows.Forms.Button btnFight;
        private System.Windows.Forms.Label lblListLocation;
    }
}

